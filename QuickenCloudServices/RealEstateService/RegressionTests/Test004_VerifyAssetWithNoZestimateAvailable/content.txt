!***< Import fixtures

| import |
| qkn.fitnesse.fixture |
| smartrics.rest.fitnesse.fixture.RestFixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!


!1 Verify Asset Search is successful for address search
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/realestate/assets/search?query=${addressQuerySearch_NoZestimate}|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| | 200 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : #IGNORE
Connection : keep-alive
}}}| {{{
{
    "metaData": {
        "totalSize": 1,
        "offset": 0,
        "limit": 500,
        "pageSize": 0,
        "currentPage": 0
    },
    "resources": [{
        "zipCode": "95118",
        "deleted": false,
        "city": "San Jose",
        "assetDetailsList": [],
        "state": "CA",
        "line1": "4951 Cherry Ave"
    }]
}
}}} |!- line1_Response = RESPONSE[resources[0].line1]
state_Response = RESPONSE[resources[0].state]
city_Response = RESPONSE[resources[0].city]
zipCode_Response = RESPONSE[resources[0].zipCode] -! |



!1 Verify POST Asset is successful
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/realestate/assets/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{
    "line1": "%line1_Response%",
    "city":"%city_Response%",
    "state":"%state_Response%",
    "zipCode": "%zipCode_Response%"
} 
}}}| 201 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Date : #IGNORE
Expires : 0
Location : #IGNORE
qcs_resource_id : #IGNORE
Connection : keep-alive
}}}| |!- Resource_id= HEADER[qcs_resource_id] -! |



!1 Verify GET Asset is successful for asset with NO ZESTIMATE
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/realestate/assets/%Resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| | 200 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : #IGNORE
Connection : keep-alive
}}}|{{{
{
    "createdAt": "#IGNORE",
    "zipCode": "%zipCode_Response%",
    "deleted": false,
    "city": "%city_Response%",
    "modifiedAt": "#IGNORE",
  "assetDetailsList": [{
       "bedrooms": "3",
        "useCode": "Apartment",
        "thirdPartyName": "Zillow",
        "thirdPartyPropertyID": "82958168",
        "detailsURL": "http://www.zillow.com/homedetails/4951-Cherry-Ave-San-Jose-CA-95118/82958168_zpid/?view=owner&utm_source=quicken",
        "lastUpdatedThirdParty": "12/31/1969",
        "finishedSqFt": "675",
        "bathrooms": "2.5"
    }],
    "id": "%Resource_id%",
    "state": "%state_Response%",
    "line1": "%line1_Response%"
}
}}} | |


!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.RealEstateService.DeleteAnAsset


