!***< Import fixtures

| import |
| qkn.fitnesse.fixture |
| smartrics.rest.fitnesse.fixture.RestFixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!


!1 Verify Asset Search is successful for asset
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/realestate/assets/search?query=${addressQuerySearch_MultiFamily}|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| | 200 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Connection : keep-alive
}}}| {{{
{
    "metaData": {
        "totalSize": 1,
        "offset": 0,
        "limit": 500,
        "pageSize": 0,
        "currentPage": 0
    },
    "resources": [{
        "zipCode": "33606",
        "deleted": false,
        "city": "Tampa",
        "assetDetailsList": [],
        "state": "FL",
        "line1": "404 S Westland Ave APT 1"
    }]
}
}}} |!- line1_Response = RESPONSE[resources[0].line1]
state_Response = RESPONSE[resources[0].state]
city_Response = RESPONSE[resources[0].city]
zipCode_Response = RESPONSE[resources[0].zipCode] -! |



!1 Verify POST Asset is successful
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/realestate/assets/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{
    "line1": "%line1_Response%",
    "city":"%city_Response%",
    "state":"%state_Response%",
    "zipCode": "%zipCode_Response%"
} 
}}}| 201 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Date : #IGNORE
Expires : 0
Location : #IGNORE
qcs_resource_id : #IGNORE
Connection : keep-alive
}}}| |!- Resource_id= HEADER[qcs_resource_id] -! |



!1 Verify GET Asset is successful for multi family asset
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/realestate/assets/%Resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| | 200 | {{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Connection : keep-alive
}}}|{{{
{
    "createdAt": "#IGNORE",
    "zipCode": "%zipCode_Response%",
    "deleted": false,
    "city": "%city_Response%",
    "modifiedAt": "#IGNORE",
    "assetDetailsList": [{
        "bedrooms": "1",
        "useCode": "MultiFamily2To4",
        "thirdPartyName": "Zillow",
        "thirdPartyPropertyID": "2108861439",
        "detailsURL": "http://www.zillow.com/homedetails/404-S-Westland-Ave-APT-1-Tampa-FL-33606/2108861439_zpid/?view=owner&utm_source=quicken",
        "lastUpdatedThirdParty": "#NOTNULL",
        "finishedSqFt": "540",
        "bathrooms": "1.0",
        "value": "#NOTNULL"
    }],
    "id": "%Resource_id%",
    "state": "%state_Response%",
    "line1": "%line1_Response%"
}
}}} | |




!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.RealEstateService.DeleteAnAsset
