!***< Import fixtures

| import |
| qkn.fitnesse.fixture |

*!
!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers | !- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID] |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken] |

*!
!1 URL Service Request Appropriate Query Parameter & AppropriatePlatformInfo
----
| Table: qkn Rest Fixture | https://services-qa.quickencs.com |
| Method | Url | Request Headers | Request Payload | Status | Response Headers | Response Payload | Variable |
| GET | /urls/tt_amazon?platform=mac| |  | 200 |  |{{{
{
    "urllink": "https://www.quicken.com/turbotax/amazon",
    "urlname": "tt_amazon"
}
}}} | |
