!***< Import fixtures

| import |
| qkn.fitnesse.fixture |

*!
!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers | !- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID] |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken] |

*!
!1 Privacy Service Request with Missing Email Info
----
| Table: qkn Rest Fixture | https://services-stg.quickencs.com |
| Method | Url | Request Headers | Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /optout/create | Content-Type:application/json |{{{
{
"enableAddress": true,
"enablePhone": true,
"enableEmail": true,
"address": {
	"name": "Priyanka",
	"line1": "4359 sloat rd",
	"line2": "",
	"city": "Fremont",
	"state": "CA",
	"zip": "94538",
	"country": "USA"
},
"phone": "+1 418-218-9889",
"email": ""
}
}}}| 400 |  |{{{
{"errors": [{
    "code": "QCS-0400-6",
    "extData": {"entries": [{
        "value": "email",
        "key": "parameterName"
    }]},
    "httpStatus": 400,
    "title": "Parameter email must be specified for this request."
}]}
}}} ||
