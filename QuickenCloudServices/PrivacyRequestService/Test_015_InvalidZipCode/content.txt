!***< Import fixtures

| import |
| qkn.fitnesse.fixture |

*!
!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers | !- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID] |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken] |

*!
!1 Privacy Service Request with Invalid Zipcode 
----
| Table: qkn Rest Fixture | https://services-stg.quickencs.com |
| Method | Url | Request Headers | Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /optout/create | Content-Type:application/json |{{{
{
"enableAddress": true,
"enablePhone": false,
"enableEmail": false,
"address": {
	"name": "Priyanka",
	"line1": "4359 sloat rd",
	"line2": "",
	"city": "Fremont",
	"state": "CA",
	"zip": "zzz",
	"country": "USA"
},
"phone": "4082189898",
"email": "pri@gmail.com"
}
}}}| 400 |  |{{{
{"errors": [{
    "code": "QCS-0400-2",
    "httpStatus": 400,
    "detail": "Could not read document: Can not construct instance of java.lang.Integer from String value 'zzz': not a valid Integer value\n at [Source: java.io.PushbackInputStream@83be019; line: 11, column: 15] (through reference chain: com.quicken.privacy.resources.OptoutResource[\"address\"]->com.quicken.privacy.resources.Address[\"zip\"]); nested exception is com.fasterxml.jackson.databind.exc.InvalidFormatException: Can not construct instance of java.lang.Integer from String value 'zzz': not a valid Integer value\n at [Source: java.io.PushbackInputStream@83be019; line: 11, column: 15] (through reference chain: com.quicken.privacy.resources.OptoutResource[\"address\"]->com.quicken.privacy.resources.Address[\"zip\"])",
    "title": "Invalid request payload."
}]}
}}} | |
