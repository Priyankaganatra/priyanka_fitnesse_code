!***< Import fixtures

| import |
| qkn.fitnesse.fixture |

*!
!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers | !- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID] |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken] |

*!
!1 Privacy Service Request with Invalid City Name 
----
| Table: qkn Rest Fixture | https://services-stg.quickencs.com |
| Method | Url | Request Headers | Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /optout/create | Content-Type:application/json |{{{
{
"enableAddress": true,
"enablePhone": false,
"enableEmail": false,
"address": {
	"name": "Priyanka",
	"line1": "4359 sloat rd",
	"line2": "",
	"city": "1111",
	"state": "CA",
	"zip": "94538",
	"country": "USA"
},
"phone": "4082189898",
"email": "pri@gmail.com"
}
}}}| 200 |  |{{{
Optout Details Created
}}} | |
