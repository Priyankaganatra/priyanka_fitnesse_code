!define TEST_SYSTEM {slim}
!1 Smoke test
'''Author- Priyanka(PG)'''
'''LastModified- 08/01/2016'''
!contents -R2 -g -p -f -h
!***> Class Path & Global Defines
'''Variables Settings'''
!define COLLAPSE_SETUP {true}
!define COLLAPSE_TEARDOWN {true}
!path ${java.class.path}



!define host_QAM_localHost {http://192.168.99.100:10102}
!define host_QA {https://services-qa.quickencs.com}
!define Environment {QAEnvironment}
!define ExistingUserToken {ExistingUserQAEnv}
!define qcsDatasetIdKey {qcs-dataset-id}
!define qcsDatasetIdValue {23734190138070016}
!define metadataLimit {500}


!define slim.timeout {120000}



!define slim.timeout {12000}


*!