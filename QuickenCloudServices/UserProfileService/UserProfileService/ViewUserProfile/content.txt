!***< Import fixtures


| import |
| qkn.fitnesse.fixture |


*!
!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.UserProfileService.UserProfileService.CreateQamUser

!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!


!2 Get the user profile 
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET | /userprofiles/${qcs_resource_id} | %accessToken%| | 200 | {{{
Cache-Control : #IGNORE
Content-Type : application/json;charset=UTF-8
Date : #IGNORE
Connection : keep-alive
}}}|{{{
{
    "createdAt": "#IGNORE",
    "firstName": "FitNesse",
    "lastName": "User",
    "primaryPhone": {
        "createdAt": "#IGNORE",
        "number": "7895463211",
        "modifiedAt": "#IGNORE",
        "countryCode": "1",
        "verified": false,
        "authUse": false,
        "id": "23524176890827776",
        "type": "MOBILE",
        "primary": true
    },
    "modifiedAt": "#IGNORE",
    "id": "18602303065297920",
    "primaryAddress": {
        "country": "usa",
        "zipCode": "92923",
        "defaultForShipping": false,
        "city": "bang",
        "modifiedAt": "#IGNORE",
        "verified": false,
        "fullName": "fullname test12",
        "authUse": false,
        "createdAt": "#IGNORE",
        "defaultForBilling": false,
        "id": "23524176857273344",
        "state": "ca",
        "line1": "1234",
        "primary": true
    },
    "userId": "18602301186249728",
    "primaryEmail": {
        "createdAt": "#IGNORE",
        "address": "fitnessetestuserqa@mailinator.com",
        "modifiedAt": "#IGNORE",
        "verified": false,
        "description": "my regular email",
        "authUse": false,
        "id": "18602303082075136",
        "primary": true
    },
    "username": "fitnessetestuserqa@mailinator.com"
}
}}} |!- user_id= RESPONSE[id]
primaryAddress_id = RESPONSE[primaryAddress.id]
primaryEmail_id = RESPONSE[primaryEmail.id]
primaryPhone_id = RESPONSE [primaryPhone.id] 
userId = RESPONSE [userId]-! |
