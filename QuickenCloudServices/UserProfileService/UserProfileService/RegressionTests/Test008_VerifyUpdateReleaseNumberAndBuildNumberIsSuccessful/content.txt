!***< Import fixtures


| import |
| qkn.fitnesse.fixture |

*!


!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 50000 |
| http.socket.timeout | 20000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${EnvironmentToken}, null), accessToken]|


*!

!1 Verify POST Product usage started is successfully added
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /products/usageStarted |%accessToken%| {{{
{

"version": "${version}",
"sku": "${sku}", 
"flavor": "${flavor}",
"releaseNumber": "${update_ReleaseNumber}", 
"buildNumber": "${update_BuildNumber}", 
"clientType": "${clientType}", 
"productAlias": "${productAlias}",
"prevQuickenUser": "${update_PrevQuickenUser}",
"channel" : "${update_Channel}"
  
}
}}} | 204 | {{{Cache-Control : #IGNORE
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
Connection : #IGNORE
}}} | |phone_qcs_resource_id = HEADER[qcs_resource_id]|


!3 Verify GET /products for the usage is successful
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET | /products |%accessToken%| | 200 | {{{Cache-Control : #IGNORE
Content-Type : application/json;charset=UTF-8
Date : #IGNORE
Expires : #IGNORE
Connection : #IGNORE
}}} |{{{
{
  "resources": [
    {
      "id": "28137869582668800",
      "modifiedAt": "#IGNORE",
      "createdAt": "#IGNORE",
      "version": "${version}",
      "sku": "${sku}",
      "flavor": "${flavor}",
      "releaseNumber": "${update_ReleaseNumber}",
      "buildNumber": "${update_BuildNumber}",
      "productAlias": "${productAlias}",
      "clientType": "${clientType}",
      "channel": "${update_Channel}",
      "prevQuickenUser": "${update_PrevQuickenUser}"
    }
  ],
  "metaData": {
    "asOf": "#IGNORE",
    "totalSize": 1,
    "offset": 1,
    "limit": 500,
    "lastRefId": "28137869582668800",
    "pageSize": 1,
    "currentPage": 1
  }
}
}}} | |



!2 Reset Product usage started
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /products/usageStarted |%accessToken%| {{{
{

"version": "${version}",
"sku": "${sku}", 
"flavor": "${flavor}",
"releaseNumber": "${releaseNumber}", 
"buildNumber": "${update_BuildNumber}", 
"clientType": "${clientType}", 
"productAlias": "${productAlias}",
"prevQuickenUser": "${prevQuickenUser}",
"channel" : "${channel}"
  
}
}}} | 204 | {{{Cache-Control : #IGNORE
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
Connection : #IGNORE
}}} | |phone_qcs_resource_id = HEADER[qcs_resource_id]|

