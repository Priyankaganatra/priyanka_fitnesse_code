!***< Import fixtures


| import |
| qkn.fitnesse.fixture |

*!


!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 50000 |
| http.socket.timeout | 20000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
|%clientType%|WINDOWS|

*!


!1 Verify GET /products with Client Type filters is successful
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET | /products?query=(clientType=="%clientType%") |%accessToken%| | 200 | {{{Cache-Control : #IGNORE
Content-Type : application/json;charset=UTF-8
Date : #IGNORE
Expires : #IGNORE
Connection : #IGNORE
}}} |{{{
{
    "metaData": {
        "totalSize": 4,
        "asOf": "#IGNORE",
        "offset": 4,
        "limit": 500,
        "pageSize": 4,
        "lastRefId": "#IGNORE",
        "currentPage": 1
    },
    "resources": [
        {
            "flavor": "#IGNORE",
            "createdAt": "#IGNORE",
            "clientType": "%clientType%",
            "modifiedAt": "#IGNORE",
            "channel": "#IGNORE",
            "id": "#IGNORE",
            "sku": "#IGNORE",
            "releaseNumber": "#IGNORE",
            "version": "#IGNORE",
            "buildNumber": "#IGNORE",
            "prevQuickenUser": "#IGNORE",
            "productAlias": "#IGNORE"
        },
        {
            "flavor": "#IGNORE",
            "createdAt": "#IGNORE",
            "clientType": "%clientType%",
            "modifiedAt": "#IGNORE",
            "channel": "#IGNORE",
            "id": "#IGNORE",
            "sku": "#IGNORE",
            "releaseNumber": "#IGNORE",
            "version": "#IGNORE",
            "buildNumber": "#IGNORE",
            "prevQuickenUser": "#IGNORE",
            "productAlias": "#IGNORE"
        },
        {
            "flavor": "#IGNORE",
            "createdAt": "#IGNORE",
            "clientType": "%clientType%",
            "modifiedAt": "#IGNORE",
            "channel": "#IGNORE",
            "id": "#IGNORE",
            "sku": "#IGNORE",
            "releaseNumber": "#IGNORE",
            "version": "#IGNORE",
            "buildNumber": "#IGNORE",
            "prevQuickenUser": "#IGNORE",
            "productAlias": "#IGNORE"
        },
        {
            "flavor": "#IGNORE",
            "createdAt": "#IGNORE",
            "clientType": "%clientType%",
            "modifiedAt": "#IGNORE",
            "id": "#IGNORE",
            "sku": "#IGNORE",
            "releaseNumber": "#IGNORE",
            "version": "#IGNORE",
            "buildNumber": "#IGNORE",
            "prevQuickenUser": "#IGNORE",
            "productAlias": "#IGNORE"
        }
    ]
}
}}} | |

