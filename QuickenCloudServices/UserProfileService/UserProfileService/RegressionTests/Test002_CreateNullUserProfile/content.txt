!***< Import fixtures


| import |
| qkn.fitnesse.fixture |
| smartrics.rest.fitnesse.fixture.RestFixture |
*!


!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 50000 |
| http.socket.timeout | 20000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|

*!


!1 Create a user profile 
----
| Table: qkn Rest Fixture | ${Host_QAM} |
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST | /userprofiles |%accessToken%| {{{
{ "firstName":"",
  "lastName":"",
  "userName":"",
  
  
  "primaryAddress":{"fullName":"",
    "line1":"",
    "city":"", 
    "state":"", 
    "country":"", 
    "zipCode":""
    }
    ,"primaryPhone":{"number":"", "type":""},
    "primaryEmail":{"address":"","description":""}
}


}}} | 400 |  {{{Cache-Control : #IGNORE
Content-Type : application/json;charset=UTF-8
Date : #IGNORE
Expires : #IGNORE
Connection : #IGNORE }}}| | qcs_resource_id = HEADER[qcs_resource_id]  |
