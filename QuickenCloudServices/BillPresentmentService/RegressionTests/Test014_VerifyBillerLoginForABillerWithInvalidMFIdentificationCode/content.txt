!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %userName% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomUserEmail(), fitnessetestuser, qcsfitnesse]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!

!1 Verify add biller login for a biller with Invalid MFA Identification Code - Credit


!1 Get the proofs for the biller
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers?query=(providerBillerID=="${providerID_DiscoverCard}") |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "providerBillerID": "${providerID_DiscoverCard}",
    "websiteURL": "#NOTNULL",
    "credentials": [
        {
            "examples": [],
            "description": "User ID",
            "type": "USERNAME",
            "choices": [],
            "status": "UNKNOWN"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "UNKNOWN"
        }
    ],
    "accountType": "${accountType_credit}",
    "name": "#CONTAINS[${billerSearch_8}]",
    "refreshingEnabled": ${refreshingEnabled_True}
}
}}} | |






!1 Add Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_DiscoverCard}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}"
	] 
	
}
}}} |201 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| |!- qcs_resource_id = HEADER[qcs_resource_id] -! |



|fitnesse.fixtures.Sleep|20000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#IGNORE",
    "isDeleted": false,
    "providerBillerID": "${providerID_DiscoverCard}",
    "credentials": [
        {
            "examples": [],
            "description": "User ID",
            "type": "USERNAME",
            "choices": [],
            "status": "UNVERIFIED"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "UNVERIFIED"
        },
       {
      "type": "QUESTION_ANSWER_CHOICE",
      "description": "Select a delivery mechanism for receiving an ID code",
      "status": "MISSING",
      "examples": [],
      "choices": [
        "SMS text",
        "Email",
        "Voice call"
      ]
    }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_credit}",
    "name": "${billerSearch_8}",
    "refreshStatus": "CREDENTIALS_MISSING",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL",
    "notice": "#CONTAINS[account needs you to provide some additional details so that we can continue to sync your info]",
}}}} | |




!1 Enter Identification code mode for Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| PUT |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_DiscoverCard}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}",
        "${proof_MFA3}"

	] 
	
}
}}} |204 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : %qcs_resource_id%
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| | |


|fitnesse.fixtures.Sleep|30000|

!1 Verify MFA Proofs have been validated successfully
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_DiscoverCard}",
    "credentials": [
    {
      "type": "USERNAME",
      "description": "User ID",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": []
    },
    {
      "type": "PASSWORD",
      "description": "Password",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": []
    },
    {
      "type": "QUESTION_ANSWER_CHOICE",
      "description": "Select a delivery mechanism for receiving an ID code",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": [
        "SMS text",
        "Email",
        "Voice call"
      ]
    },
    {
      "type": "QUESTION_ANSWER",
      "description": "Identification Code (delivered to SMS text)",
      "status": "MISSING",
      "examples": [],
      "choices": []
    }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_credit}",
    "name": "${billerSearch_8}",
    "refreshStatus": "CREDENTIALS_MISSING",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL",
    "notice": "#CONTAINS[account needs you to provide some additional details so that we can continue to sync your info]",
}}}} | |






!1 Enter Invalid Identification code for Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| PUT |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_DiscoverCard}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}",
        "${proof_MFA3}",
        "invalidCode"

	] 
	
}
}}} |204 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : %qcs_resource_id%
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| | |


|fitnesse.fixtures.Sleep|20000|

!1 Verify MFA Proofs have been validated successfully
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_DiscoverCard}",
    "credentials": [
    {
      "type": "USERNAME",
      "description": "User ID",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": []
    },
    {
      "type": "PASSWORD",
      "description": "Password",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": []
    },
    {
      "type": "QUESTION_ANSWER_CHOICE",
      "description": "Select a delivery mechanism for receiving an ID code",
      "status": "UNVERIFIED",
      "examples": [],
      "choices": [
        "SMS text",
        "Email",
        "Voice call"
      ]
    },
    {
      "type": "QUESTION_ANSWER",
      "description": "Identification Code (delivered to SMS text)",
      "status": "INVALID",
      "examples": [],
      "choices": []
    }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_credit}",
    "name": "${billerSearch_8}",
    "refreshStatus": "CREDENTIALS_INVALID",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |




!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.BillPresentmentService.DeleteBillerLogins




