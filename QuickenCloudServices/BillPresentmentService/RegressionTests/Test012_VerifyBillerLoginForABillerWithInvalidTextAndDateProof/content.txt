!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %userName% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomUserEmail(), fitnessetestuser, qcsfitnesse]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!

!1 Verify add biller login for a biller with invalid text and date proof is un-successful


!1 Get the proofs for the biller
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers?query=(providerBillerID=="${providerID_SallieMae}") |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200|{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "providerBillerID": "${providerID_SallieMae}",
    "websiteURL": "#NOTNULL",
    "credentials": [
        {
            "examples": [],
            "description": "User ID",
            "type": "USERNAME",
            "choices": [],
            "status": "UNKNOWN"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "UNKNOWN"
        },
    {
      "type": "TEXT",
      "pattern": "^([\\d]{3})-([\\d]{2})-([\\d]{4})$",
      "description": "Social Security Number",
      "status": "UNKNOWN",
      "examples": [
        "123-45-6789"
      ],
      "choices": []
    },
    {
      "type": "DATE",
      "pattern": "^[\\d]{2}\\/[\\d]{2}\\/[\\d]{4}$",
      "description": "Date of Birth",
      "status": "UNKNOWN",
      "examples": [
        "01/01/2000"
      ],
      "choices": []
    }
    ],
    "accountType": "${accountType_StudentLoan}",
    "name": "#CONTAINS[${billerSearch_6}]",
    "refreshingEnabled": ${refreshingEnabled_True}
}
}}} | |






!1 Add Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_SallieMae}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}",
        "invalidproof",
        "invalidpwd"
	] 
	
}
}}} |400 |{{{
Date : #IGNORE
Expires : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}|{{{{"errors": [{
    "code": "QCS-0400-8",
    "extData": {"entries": [
        {"key": "parameterName"},
        {"key": "parameterValue"}
    ]},
    "httpStatus": 400,
    "detail": "Invalid value for parameter [parameter=null] because The value you provided is not in the correct format. Please double check that you've entered it correctly.",
    "title": "Invalid value null for parameter null."
}]}}}} | |






!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.BillPresentmentService.DeleteBillerLogins




