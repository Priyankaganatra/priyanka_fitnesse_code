!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %userName% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomUserEmail(), fitnessetestuser, qcsfitnesse]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
| %dueDate% | #FUNCTION[qkn.fitnesse.utils.DateGenerator, getDefaultDueDate(${dueDate_upcoming},MM/dd/yyyy), dueDate]|
| %dueDateOutput% | #FUNCTION[qkn.fitnesse.utils.DateGenerator, getDefaultDueDate(${dueDate_upcoming},yyyy-MM-dd), dueDateOutput]|
*!

!1 Verify biller login refresh returns Invalid credentials - Insurance Biller


!1 Get the proofs for the biller
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers?query=(providerBillerID=="${providerID_ProgressiveInsurance}") |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "providerBillerID": "${providerID_ProgressiveInsurance}",
    "websiteURL": "#NOTNULL",
    "credentials": [
      {
      "type": "USERNAME",
      "description": "Username",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    },
    {
      "type": "PASSWORD",
      "description": "Password",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    }
      ],
    "accountType": "${accountType_Insurance}",
    "name": "#CONTAINS[${billerSearch_24}]",
    "refreshingEnabled": ${refreshingEnabled_True}
}
}}} | |






!1 Add Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_ProgressiveInsurance}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}"
	]
	
}
}}} |201 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| |!- qcs_resource_id = HEADER[qcs_resource_id] -! |



|fitnesse.fixtures.Sleep|30000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_ProgressiveInsurance}",
    "credentials": [
        {
            "examples": [],
            "description": "Username",
            "type": "USERNAME",
            "choices": [],
            "status": "VALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "VALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_Insurance}",
    "name": "#CONTAINS[${billerSearch_24}]",
    "refreshStatus": "REFRESHED",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |



!1 Verify GET Biller Accounts is null
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billeraccounts?query=(billerLoginID=="%qcs_resource_id%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "metaData": {
        "totalSize": 1,
        "asOf": "#IGNORE",
        "offset": 1,
        "limit": 500,
        "pageSize": 1,
        "lastRefId": "#IGNORE",
        "currentPage": 1
    },
    "resources": [{
        "createdAt": "#IGNORE",
        "billerLoginID": "%qcs_resource_id%",
        "modifiedAt": "#IGNORE",
        "nickName": "Motorcyle Insurance",
        "name": "#NOTNULL",
        "id": "#NOTNULL",
        "providerBillerAccountID": "#IGNORE"
    }]
}
}}} |!- id_billerAccounts = RESPONSE[resources.id[0]] -!  |



!1 Refresh Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/%qcs_resource_id%/refresh |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_ProgressiveInsurance}",
    "credentials": [
        {
            "examples": [],
            "description": "Username",
            "type": "USERNAME",
            "choices": [],
            "status": "VALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "VALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_Insurance}",
    "name": "#CONTAINS[${billerSearch_24}]",
    "refreshStatus": "REFRESHING",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |


|fitnesse.fixtures.Sleep|30000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#IGNORE",
    "isDeleted": false,
    "providerBillerID": "${providerID_ProgressiveInsurance}",
    "credentials": [
        {
            "examples": [],
            "description": "Username",
            "type": "USERNAME",
            "choices": [],
            "status": "INVALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "INVALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_Insurance}",
    "name": "#CONTAINS[${billerSearch_24}]",
    "refreshStatus": "CREDENTIALS_INVALID",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL",
    "notice": "#CONTAINS[account needs you to update some details. Some of the login details you provided may not be correct.]"

}}}} | |


!1 Update Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| PUT |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_ProgressiveInsurance}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}"
	]
	
}
}}} |204 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| |!- qcs_resource_id = HEADER[qcs_resource_id] -! |



|fitnesse.fixtures.Sleep|30000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_ProgressiveInsurance}",
    "credentials": [
        {
            "examples": [],
            "description": "Username",
            "type": "USERNAME",
            "choices": [],
            "status": "VALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "VALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_Insurance}",
    "name": "#CONTAINS[${billerSearch_24}]",
    "refreshStatus": "REFRESHED",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |


!1 Verify GET Biller Accounts
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billeraccounts?query=(billerLoginID=="%qcs_resource_id%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "metaData": {
        "totalSize": 1,
        "asOf": "#IGNORE",
        "offset": 1,
        "limit": 500,
        "pageSize": 1,
        "lastRefId": "#IGNORE",
        "currentPage": 1
    },
    "resources": [{
        "createdAt": "#IGNORE",
        "billerLoginID": "%qcs_resource_id%",
        "modifiedAt": "#IGNORE",
        "nickName": "Motorcyle Insurance",
        "name": "#NOTNULL",
        "id": "#NOTNULL",
        "providerBillerAccountID": "#NOTNULL"
    }]
}
}}} |!- id_billerAccounts = RESPONSE[resources.id[0]] -!  |



!1 Verify GET latest bills
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/bills/latest?query=(billerAccountID=="%id_billerAccounts%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "metaData": {
        "totalSize": 1,
        "asOf": "#NOTNULL",
        "offset": 1,
        "limit": 500,
        "pageSize": 1,
        "lastRefId": "#NOTNULL",
        "currentPage": 1
    },
    "resources": [{
        "amountDue": "#NOTNULL",
        "createdAt": "#NOTNULL",
        "modifiedAt": "#NOTNULL",
        "dueDate": "#NOTNULL",
        "billStatus": "#NOTNULL",
        "id": "#NOTNULL",
        "billerAccountID": "%id_billerAccounts%"
    }]
}
}}} | |



!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.BillPresentmentService.DeleteBillerLogins



