!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %userName% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomUserEmail(), fitnessetestuser, qcsfitnesse]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
| %dueDate% | #FUNCTION[qkn.fitnesse.utils.DateGenerator, getDefaultDueDate(${dueDate_fiveDays},MM/dd/yyyy), dueDate]|
| %dueDateOutput% | #FUNCTION[qkn.fitnesse.utils.DateGenerator, getDefaultDueDate(${dueDate_fiveDays},yyyy-MM-dd), dueDateOutput]|
| %lastPaidDateOutput% | #FUNCTION[qkn.fitnesse.utils.DateGenerator, getLastMonthDate(), lastPaidDateOutput]|

*!

!1 Verify latest bill with amount due in 5 days for a biller account - Credit card

!1 Get the proofs for the biller
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers?query=(providerBillerID=="${providerID_WalmartCreditCard}") |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "providerBillerID": "${providerID_WalmartCreditCard}",
    "websiteURL": "#NOTNULL",
    "credentials": [
      {
      "type": "USERNAME",
      "description": "User ID",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    },
    {
      "type": "PASSWORD",
      "description": "Password",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    }
      ],
    "accountType": "${accountType_credit}",
    "name": "#CONTAINS[${billerSearch_15}]",
    "refreshingEnabled": ${refreshingEnabled_True}
}
}}} | |






!1 Add Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_WalmartCreditCard}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}"
	]
	
}
}}} |201 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| |!- qcs_resource_id = HEADER[qcs_resource_id] -! |



|fitnesse.fixtures.Sleep|30000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_WalmartCreditCard}",
    "credentials": [
        {
            "examples": [],
            "description": "User ID",
            "type": "USERNAME",
            "choices": [],
            "status": "VALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "VALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_credit}",
    "name": "#CONTAINS[${billerSearch_15}]",
    "refreshStatus": "REFRESHED",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |


!1 Verify GET Biller Accounts
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billeraccounts?query=(billerLoginID=="%qcs_resource_id%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
  "resources": [
    {
      "id": "#IGNORE",
      "modifiedAt": "#IGNORE",
      "createdAt": "#IGNORE",
      "providerBillerAccountID": "#NOTNULL",
      "billerLoginID": "%qcs_resource_id%",
      "name": "#NOTNULL",
      "nickName": "Store Card"
    }
  ],
  "metaData": {
    "asOf": "#NOTNULL",
    "totalSize": 1,
    "offset": 1,
    "limit": 500,
    "lastRefId": "#IGNORE",
    "pageSize": 1,
    "currentPage": 1
  }
}
}}} |!- id_billerAccounts = RESPONSE[resources.id[0]] -!  |



!1 Verify GET latest bills
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/bills/latest?query=(billerAccountID=="%id_billerAccounts%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "metaData": {
        "totalSize": 1,
        "asOf": "#IGNORE",
        "offset": 1,
        "limit": 500,
        "pageSize": 1,
        "lastRefId": "#IGNORE",
        "currentPage": 1
    },
    "resources": [{
        "amountDue": "#NOTNULL",
        "createdAt": "#IGNORE",
        "balance": "#NOTNULL",
        "modifiedAt": "#IGNORE",
        "dueDate": "%dueDateOutput%",
        "billStatus": "${billStatus_unPaid}",
        "id": "#NOTNULL",
        "billerAccountID": "%id_billerAccounts%",
        "minimumDue": "#NOTNULL"
    }]
}
}}} | |



!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.BillPresentmentService.DeleteBillerLogins



