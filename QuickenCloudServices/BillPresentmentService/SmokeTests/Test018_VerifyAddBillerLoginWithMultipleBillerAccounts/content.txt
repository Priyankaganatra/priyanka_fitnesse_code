!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %userName% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomUserEmail(), fitnessetestuser, qcsfitnesse]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!

!1 Verify add biller login with multiple biller accounts is successful - Credit Card


!1 Get the proofs for the biller
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers?query=(providerBillerID=="${providerID_KohlsCreditCard}") |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "providerBillerID": "${providerID_KohlsCreditCard}",
    "websiteURL": "#NOTNULL",
    "credentials": [
      {
      "type": "USERNAME",
      "description": "User ID",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    },
    {
      "type": "PASSWORD",
      "description": "Password",
      "status": "UNKNOWN",
      "examples": [],
      "choices": []
    }
      ],
    "accountType": "${accountType_credit}",
    "name": "#CONTAINS[${billerSearch_11}]",
    "refreshingEnabled": ${refreshingEnabled_True}
}
}}} | |






!1 Add Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| POST |/billpresentment/billerlogins/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}|{{{
{ 
	"providerBillerID": "${providerID_KohlsCreditCard}", 
"credentialAnswers": 
	[ 
	"%userName%", 
	"${proof_Password}"
	] 
	
}
}}} |201 |{{{
Date : #IGNORE
Expires : #IGNORE
Location : #IGNORE
qcs_resource_id : #IGNORE
X-Application-Context : #IGNORE
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
}}}| |!- qcs_resource_id = HEADER[qcs_resource_id] -! |



|fitnesse.fixtures.Sleep|40000|

!1 GET Biller login
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| | |
| GET |/billpresentment/billerlogins/%qcs_resource_id% |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{{
    "createdAt": "#IGNORE",
    "billerID": "#NOTNULL",
    "isDeleted": false,
    "providerBillerID": "${providerID_KohlsCreditCard}",
    "credentials": [
        {
            "examples": [],
            "type": "USERNAME",
            "description": "User ID",
            "choices": [],
            "status": "VALID"
        },
        {
            "examples": [],
            "description": "Password",
            "type": "PASSWORD",
            "choices": [],
            "status": "VALID"
        }
    ],
    "modifiedAt": "#IGNORE",
    "providerBillerLoginID": "#NOTNULL",
    "accountType": "${accountType_credit}",
    "name": "Kohl's",
    "refreshStatus": "REFRESHED",
    "id": "%qcs_resource_id%",
    "providerUserID": "#NOTNULL"
}}}} | |


!1 Verify GET Biller Accounts
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billeraccounts?query=(billerLoginID=="%qcs_resource_id%")|%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
    "metaData": {
        "totalSize": 3,
        "asOf": "#IGNORE",
        "offset": 3,
        "limit": 500,
        "pageSize": 3,
        "lastRefId": "#NOTNULL",
        "currentPage": 1
    },
    "resources": [
        {
            "createdAt": "#IGNORE",
            "billerLoginID": "%qcs_resource_id%",
            "modifiedAt": "#IGNORE",
            "nickName": "",
            "name": "1111",
            "id": "#NOTNULL",
            "providerBillerAccountID": "#NOTNULL"
        },
        {
            "createdAt": "#IGNORE",
            "billerLoginID": "%qcs_resource_id%",
            "modifiedAt": "#IGNORE",
            "nickName": "",
            "name": "3333",
            "id": "#NOTNULL",
            "providerBillerAccountID": "#NOTNULL"
        },
        {
            "createdAt": "#IGNORE",
            "billerLoginID": "%qcs_resource_id%",
            "modifiedAt": "#IGNORE",
            "nickName": "",
            "name": "2222",
            "id": "#NOTNULL",
            "providerBillerAccountID": "#NOTNULL"
        }
    ]
}
}}} |!- id_billerAccounts = RESPONSE[id] -!  |

!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.BillPresentmentService.DeleteBillerLogins



