!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!



!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!


!1 Verify popular biller search
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/billpresentment/billers/popular |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue}| |200 |{{{
Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}|{{{
{
  "resources": [
    {
      "providerBillerID": "67844372cde749abb52c9b51168e0a78",
      "name": "Capital One Credit Card",
      "websiteURL": "https://www.capitalone.com/",
      "loginURL": "https://verified.capitalone.com/sic-ui/#/esignin?Product=Card",
      "resetPasswordURL": "https://login.capitalone.com/loginweb/forgotidm/forgotpass.do",
      "createAccountURL": "https://login.capitalone.com/loginweb/enrollidm/enrollWelcome.do",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "5554882d79f94480999ca610e03c4bcb",
      "name": "Chase",
      "websiteURL": "https://www.chase.com/",
      "loginURL": "https://www.chase.com/",
      "resetPasswordURL": "https://chaseonline.chase.com/Public/Reidentify/ReidentifyFilterView.aspx?LOB=RBGLogon",
      "createAccountURL": "https://chaseonline.chase.com/public/enroll/IdentifyUser.aspx?LOB=RBGLogon",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "f3b4bbcfd0c54179bb420aa91703b922",
      "name": "Bank of America",
      "websiteURL": "https://www.bankofamerica.com/",
      "resetPasswordURL": "https://secure.bankofamerica.com/login/reset/entry/forgotPwdScreen.go?errorCode=",
      "createAccountURL": "https://secure.bankofamerica.com/login/enroll/entry/olbEnroll.go?reason=model_enroll",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Online ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Passcode",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "5681a50d7be14c19a8f16265f4bfcdfe",
      "name": "Netflix",
      "websiteURL": "https://www.netflix.com/",
      "loginURL": "https://www.netflix.com/Login",
      "resetPasswordURL": "https://www.netflix.com/LoginHelp?locale=en-US",
      "createAccountURL": "https://www.netflix.com/us-en/",
      "accountType": "MEDIA",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Email",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "df51232a0d4f414fa0600f74717f074f",
      "name": "Wells Fargo",
      "websiteURL": "https://www.wellsfargo.com/",
      "resetPasswordURL": "https://www.wellsfargo.com/help/faqs/sign-on",
      "createAccountURL": "https://online.wellsfargo.com/das/channel/enrollDisplay?LOB=CONS&OFFERCODE=WEB",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "b3480a1e4501449a935331f231961fc7",
      "name": "American Express",
      "websiteURL": "https://www.americanexpress.com/",
      "loginURL": "https://www.americanexpress.com/",
      "resetPasswordURL": "https://online.americanexpress.com/myca/fuidfyp/us/action?request_type=un_fuid&Face=en_US&entry_point=lnk_fuid&omnlogin=us_homepage_login",
      "createAccountURL": "https://online.americanexpress.com/myca/oce/us/action/register?request_type=un_Register&Face=en_US&Face=en_US&regSrc=logon",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "e8fca29bbbab45819c27268804919eed",
      "name": "Credit One Bank",
      "websiteURL": "https://www.creditonebank.com/",
      "loginURL": "https://www.creditonebank.com/",
      "resetPasswordURL": "https://www.creditonebank.com/account-reset",
      "createAccountURL": "https://www.creditonebank.com/account-setup",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "9bc938bb87c548c2923c11fd86e98b43",
      "name": "PayPal",
      "websiteURL": "https://www.paypal.com/",
      "loginURL": "http://www.paypal.com/home",
      "resetPasswordURL": "http://www.paypal.com/home",
      "createAccountURL": "https://www.paypal.com/signup/account",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Email",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "3899b311bdac490788ea236de12382e3",
      "name": "AT&T",
      "websiteURL": "http://www.att.com/",
      "resetPasswordURL": "https://www.att.com/olam/unauth/fpwdEnterUserId.myworld",
      "createAccountURL": "https://www.att.com/olam/unauth/displaySelectAccountRegistration.myworld",
      "accountType": "INTERNET_PHONE_CABLE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "4f8785f0025f4963bd19ea4e9ff6ee31",
      "name": "Barclaycard",
      "websiteURL": "https://www.barclaycardus.com/",
      "loginURL": "https://www.barclaycardus.com/servicing/home?secureLogin",
      "resetPasswordURL": "https://www.barclaycardus.com/servicing/RetrieveUsername.action?originatingCPC=003",
      "createAccountURL": "https://www.barclaycardus.com/servicing/Register.action",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "7f54955909604bdb9c373ead188e70b8",
      "name": "XFINITY",
      "websiteURL": "http://www.xfinity.com/",
      "loginURL": "#NOTNULL",
      "resetPasswordURL": "https://login.comcast.net/myaccount/reset;reset-pwd-session-id=hd45VR2Tjgc9L3T5SCb2rChwpG656c0nsm1K9KsGy27h2TCnxJbJ!205379978!-731852445?execution=e1s1",
      "createAccountURL": "https://login.comcast.net/myaccount/create-uid?execution=e2s1",
      "accountType": "INTERNET_PHONE_CABLE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username (Comcast ID or Email)",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "40e2058f738641bbafe2ca69c5e36bfb",
      "name": "Best Buy Credit Card",
      "websiteURL": "https://citiretailservices.citibankonline.com/RSnextgen/svc/launch/index.action?siteId=PLCN_BESTBUY#signon",
      "loginURL": "https://citiretailservices.citibankonline.com/RSnextgen/svc/launch/index.action?siteId=PLCN_BESTBUY#signon",
      "resetPasswordURL": "https://citiretailservices.citibankonline.com/RSnextgen/svc/launch/index.action?siteId=PLCN_BESTBUY#reset",
      "createAccountURL": "https://citiretailservices.citibankonline.com/RSnextgen/svc/registration/index.action?siteId=PLCN_BESTBUY#verify",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "b5e3f526646743058fc772b4629e184d",
      "name": "Sprint",
      "websiteURL": "http://sprint.com/",
      "loginURL": "http://m.sprint.com",
      "resetPasswordURL": "http://sprint.com/",
      "createAccountURL": "http://sprint.com/",
      "accountType": "PHONE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Username",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "97d072438f534bbaa3772b727dcb7605",
      "name": "Verizon Wireless",
      "websiteURL": "http://www.verizonwireless.com/",
      "loginURL": "https://login.verizonwireless.com/amserver/UI/Login",
      "resetPasswordURL": "https://login.verizonwireless.com/accessmanager/public/c/fp/start?goto=",
      "createAccountURL": "https://login.verizonwireless.com/accessmanager/public/c/reg/start?goto=",
      "accountType": "PHONE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID or Cell Phone Number",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "351305c96611441cbd7b73e3060fd6eb",
      "name": "Discover Card",
      "websiteURL": "https://www.discover.com/#/auth",
      "resetPasswordURL": "https://www.discover.com/forgot-info/index.html?ICMPGN=SA_LOGIN_BOX_FORGOT",
      "createAccountURL": "https://www.discover.com/register-account/index.html?ICMPGN=SA_LOGIN_BOX_REGISTER",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "pattern": "^(.{2,16})$",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [
            "A1",
            "ABC123",
            "ABC123DEF456GHI7"
          ],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "2aca4f61a82b426e85f6201332848393",
      "name": "T-Mobile",
      "websiteURL": "http://www.t-mobile.com/",
      "loginURL": "https://my.t-mobile.com/login.html",
      "resetPasswordURL": "https://my.t-mobile.com/reset-password.html",
      "createAccountURL": "https://my.t-mobile.com/sign-up.html",
      "accountType": "PHONE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Email  / Phone Number",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "754b1d6796ab47ba836491f7dc03014c",
      "name": "Walmart Credit Card",
      "websiteURL": "https://www2.onlinecreditcenter6.com/consumergen2/login.do?accountType=generic&clientId=walmart&subActionId=1000",
      "loginURL": "https://www.onlinecreditcenter6.com/consumergen2/login.do?accountType=generic&clientId=walmart&subActionId=1000",
      "resetPasswordURL": "https://www.onlinecreditcenter6.com/consumergen2/login.do?accountType=generic&clientId=walmart&subActionId=1000contact",
      "createAccountURL": "https://www.onlinecreditcenter6.com/consumergen2/conregistration.do",
      "accountType": "CREDIT",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "acf0ff0d8d8f4cde9b9b86251b6f9448",
      "name": "GEICO",
      "websiteURL": "http://www.geico.com/",
      "loginURL": "https://ecams.geico.com/ecams/login.xhtml",
      "resetPasswordURL": "https://ecams.geico.com/ecams/recovery.xhtml?el=TE9HSU4saGF2aW5nUHJvYmxlbXNMaW5r",
      "createAccountURL": "https://ecams.geico.com/ecams/activation.xhtml?el=TE9HSU4sYWN0aXZhdGlvbkxpbms",
      "accountType": "INSURANCE",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "bbfc918186154883bdbfd4f56c1d0b78",
      "name": "USAA",
      "websiteURL": "https://www.usaa.com/",
      "loginURL": "https://vlagg.usaa.com/inet/ent_logon/Logon",
      "resetPasswordURL": "https://www.usaa.com/inet/ent_proof/proofingEvent?action=Init&event=forgotPassword&wa_ref=pub_auth_nav_forgotpwd",
      "createAccountURL": "https://www.usaa.com/inet/ent_proof/proofingEvent?action=Init&event=registration&wa_ref=pub_auth_nav_register",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Online ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Passcode",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "a9759de6af594854865bc34db8ed1c46",
      "name": "Navient",
      "websiteURL": "https://www.navient.com/",
      "loginURL": "https://www.navient.com/loan-customers/",
      "resetPasswordURL": "https://login.navient.com/CALM/ForgotPassword.do",
      "createAccountURL": "https://login.navient.com/CALM/CreateNewAccount.do",
      "accountType": "STUDENT_LOAN",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "TEXT",
          "pattern": "^([\\d]{3})-([\\d]{2})-([\\d]{4})$",
          "description": "Social Security Number",
          "status": "UNKNOWN",
          "examples": [
            "123-45-6789"
          ],
          "choices": []
        },
        {
          "type": "DATE",
          "pattern": "^[\\d]{2}\\/[\\d]{2}\\/[\\d]{4}$",
          "description": "Date of Birth",
          "status": "UNKNOWN",
          "examples": [
            "01/01/2000"
          ],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "f286267a5f1d41119881b2ae4142b9d5",
      "name": "State Farm",
      "websiteURL": "http://www.statefarm.com/",
      "loginURL": "https://online2.statefarm.com/SSOLogin-Web/pages/login.xhtml",
      "resetPasswordURL": "https://online2.statefarm.com/B2CForgotPassword-Web/pages/forgotPwd.xhtml",
      "createAccountURL": "https://online2.statefarm.com/registration/register.do",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "User ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    },
    {
      "providerBillerID": "b3460e9aa4f54d7cb21d636b69fdef7e",
      "name": "U.S. Bank",
      "websiteURL": "http://www.usbank.com/",
      "loginURL": "http://www.usbank.com/index.html",
      "resetPasswordURL": "http://www.usbank.com/index.html",
      "createAccountURL": "http://www.usbank.com/index.html",
      "accountType": "BANK",
      "credentials": [
        {
          "type": "USERNAME",
          "description": "Personal ID",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        },
        {
          "type": "PASSWORD",
          "description": "Password",
          "status": "UNKNOWN",
          "examples": [],
          "choices": []
        }
      ],
      "refreshingEnabled": true
    }
  ],
  "metaData": {
    "totalSize": 22,
    "offset": 0,
    "limit": 500,
    "pageSize": 0,
    "currentPage": 0
  }
}
}}} | |
