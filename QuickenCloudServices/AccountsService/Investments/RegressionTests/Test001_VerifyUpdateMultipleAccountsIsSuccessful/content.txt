!***< Import fixtures

| import |
| qkn.fitnesse.fixture |


*!


!***> Configure the !-RestFixture-! with named configuration table of key value pairs+++

| Table: qkn Fixture Config |  
| http.client.connection.timeout | 500000 |
| http.socket.timeout | 200000 |
| restfixture.display.toggle.for.cells.larger.than | 20 |
| restfixture.default.headers |!- -! |
| %qID% | #FUNCTION[qkn.fitnesse.utils.RandomGenerator, getRandomInteger(), qID]|
| %accessToken% | #FUNCTION[qkn.fitnesse.utils.AuthHeader, getAuthHeader(${Environment}, null), accessToken]|
*!


!1 Create Multiple investment accounts -PUT 
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| PUT |/q17/accounts |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue} | {{{
{ "resources" : 

[ 
    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_401K}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive}

    },


    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Brokerage}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment}",
      "cashBalance": ${cashBalance_Positive1}

    },


    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_IRA}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Positive2}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Keogh}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Positive3}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_529}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": ${cashBalance_Positive1}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Trust}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive}

    },


    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_403B}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive3}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Simple}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive3}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Other}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Negative}

    },


    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Ugma}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Negative2}

    },

    {
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Sep}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": "${cashBalance_Large}"

    }
]
    
}
}}} | 200 | {{{Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| {{{
{"items": [
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id0%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id1%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id2%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id3%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id4%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id5%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id6%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id7%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id8%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id9%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    },
    {
        "code": 201,
        "resource": {
            "clientId": null,
            "etag": null,
            "id":"%Resource_id10%",
            "explanation": null,
            "status": "CREATED"
        },
        "errors": null
    }
]}
}}} |!- Resource_id0= RESPONSE[items.resource[0].id]
Resource_id1= RESPONSE[items.resource[1].id]
Resource_id2= RESPONSE[items.resource[2].id]
Resource_id3= RESPONSE[items.resource[3].id]
Resource_id4= RESPONSE[items.resource[4].id]
Resource_id5= RESPONSE[items.resource[5].id]
Resource_id6= RESPONSE[items.resource[6].id]
Resource_id7= RESPONSE[items.resource[7].id]
Resource_id8= RESPONSE[items.resource[8].id]
Resource_id9= RESPONSE[items.resource[9].id]
Resource_id10= RESPONSE[items.resource[10].id] -! |



!2 Update Multiple investment accounts -PUT 
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| PUT |/q17/accounts |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue} | {{{
{ "resources" : 

[ 
    {
      "id":"%Resource_id0%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Sep}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": "${cashBalance_Large}"


    },


    {
      "id":"%Resource_id1%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Ugma}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Negative2}


    },


    {
      "id":"%Resource_id2%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Keogh}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Positive3}


    },

    {
      "id":"%Resource_id3%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_IRA}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Positive2}


    },

    {
      "id":"%Resource_id4%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Trust}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive}


    },

    {
      "id":"%Resource_id5%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_529}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": ${cashBalance_Positive1}

    },


    {
      "id":"%Resource_id6%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Simple}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive3}

    },

    {
      "id":"%Resource_id7%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Other}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Negative}



    },

    {
      "id":"%Resource_id8%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_403B}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive3}


    },


    {
      "id":"%Resource_id9%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Brokerage}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment}",
      "cashBalance": ${cashBalance_Positive1}

    },

    {
      "id":"%Resource_id10%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_401K}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive}

    }
]
    
}
}}} | 200 | {{{Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| {{{
{"items": [
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id0%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id1%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id2%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id3%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id4%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id5%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
           "id":"%Resource_id6%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id7%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id8%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id9%",
            "status": "UPDATED"
        }
    },
    {
        "code": 200,
        "resource": {
            "id":"%Resource_id10%",
            "status": "UPDATED"
        }
    }
]}
}}} | |





!3 Verify GET multiple investment accounts shows updated resources
----
| Table: qkn Rest Fixture |${host_QA}|
| Method | Url | Request Headers |Request Payload | Status | Response Headers | Response Payload | Variable |
| GET |/q17/accounts/ |%accessToken%${qcsDatasetIdKey} :${qcsDatasetIdValue} | | 200 | {{{Cache-Control : no-cache, no-store, max-age=0, must-revalidate
Content-Type : application/json;charset=UTF-8
Pragma : no-cache
X-Content-Type-Options : nosniff
X-Frame-Options : DENY
X-XSS-Protection : 1; mode=block
Connection : keep-alive
}}}| {{{
{
  "resources": [

    {
      "id":"%Resource_id10%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_401K}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive}

    },
    {
      "id":"%Resource_id9%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Brokerage}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment}",
      "cashBalance": ${cashBalance_Positive1}

    },
    {
      "id":"%Resource_id8%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_403B}",
      "accountNumberLast4": "${accountNumberLast4_2}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_1}",
      "cashBalance": ${cashBalance_Positive3}


    },

    {
      "id":"%Resource_id7%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Other}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Negative}



    },
    {
      "id":"%Resource_id6%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Simple}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive3}

    },
    {
      "id":"%Resource_id5%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_529}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": ${cashBalance_Positive1}

    },
  {
      "id":"%Resource_id4%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Trust}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_Manual}",
      "cashBalance": ${cashBalance_Positive}


    },

    {
      "id":"%Resource_id3%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_IRA}",
      "accountNumberLast4": "${accountNumberLast4_3}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName_2}",
      "fiName": "${fiName_Investment_2}",
      "cashBalance": ${cashBalance_Positive2}


    },

    {
      "id":"%Resource_id2%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Keogh}",
      "accountNumberLast4": "${accountNumberLast4_4}",
      "sourceAccountName": "${sourceAccountName}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Positive3}


    },

    {
      "id":"%Resource_id1%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Ugma}",
      "accountNumberLast4": "${accountNumberLast4_5}",
      "sourceAccountName": "${sourceAccountName_2}",
      "userAccountName": "${userAccountName}",
      "fiName": "${fiName_Investment_3}",
      "cashBalance": ${cashBalance_Negative2}


    },
    {
      "id":"%Resource_id0%",
      "deleted": "${deletedFalse}",
      "investmentAccountType": "${accountType_inv_Sep}",
      "accountNumberLast4": "${accountNumberLast4}",
      "sourceAccountName": "${sourceAccountName_1}",
      "userAccountName": "${userAccountName_1}",
      "fiName": "${fiName_Investment_4}",
      "cashBalance": "${cashBalance_Large}"
    }

  ],
  "metaData": {
    "asOf": "#IGNORE",
    "totalSize": 11,
    "offset": 11,
    "limit": ${metadataLimit},
    "lastRefId": "#IGNORE",
    "pageSize": 11,
    "currentPage": 1
  }
}
}}} | |


!***< Configure the !-RestFixture-! for debug mode
!include <QuickenCloudServices.AccountsService.DeleteInvestmentAccounts


